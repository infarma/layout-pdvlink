package layout_pdvlink

import (
	"bitbucket.org/infarma/layout-pdvlink/remessasDePedidos"
	"os"
)

func GetArquivoDePedido(fileHandle *os.File) (remessasDePedidos.ArquivoDePedido, error) {
	return remessasDePedidos.GetStruct(fileHandle)
}

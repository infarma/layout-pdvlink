package remessasDePedidos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type HeaderArquivo struct {
	TipoRegistro     int32 `json:"TipoRegistro"`
	CnpjLaboratorio  int32 `json:"CnpjLaboratorio"`
	DataHoraArquivo  int32 `json:"DataHoraArquivo"`
	SequencialPedido int32 `json:"SequencialPedido"`
	CnpjDistribuidor int32 `json:"CnpjDistribuidor"`
}

func (h *HeaderArquivo) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesHeaderArquivo

	err = posicaoParaValor.ReturnByType(&h.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&h.CnpjLaboratorio, "CnpjLaboratorio")
	err = posicaoParaValor.ReturnByType(&h.DataHoraArquivo, "DataHoraArquivo")
	err = posicaoParaValor.ReturnByType(&h.SequencialPedido, "SequencialPedido")
	err = posicaoParaValor.ReturnByType(&h.CnpjDistribuidor, "CnpjDistribuidor")

	return err
}

var PosicoesHeaderArquivo = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":     {0, 1, 0},
	"CnpjLaboratorio":  {1, 15, 0},
	"DataHoraArquivo":  {15, 29, 0},
	"SequencialPedido": {29, 44, 0},
	"CnpjDistribuidor": {44, 58, 0},
}

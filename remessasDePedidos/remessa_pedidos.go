package remessasDePedidos

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	HeaderArquivo                  HeaderArquivo                    `json:"HeaderArquivo"`
	HeaderPedido                   HeaderPedido                     `json:"HeaderPedido"`
	ItemPedido                     []ItemPedido                     `json:"ItemPedido"`
	DetalhamentoEanSubstituidoItem []DetalhamentoEanSubstituidoItem `json:"DetalhamentoEanSubstituidoItem"`
	FooterArquivo                  FooterArquivo                    `json:"FooterArquivo"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:1])

		var indexItem, indexDetalhamento int32
		if identificador == "1" {
			err = arquivo.HeaderArquivo.ComposeStruct(string(runes))
		} else if identificador == "2" {
			err = arquivo.HeaderPedido.ComposeStruct(string(runes))
			indexItem++
		} else if identificador == "3" {
			err = arquivo.ItemPedido[indexItem].ComposeStruct(string(runes))
			indexItem++
		} else if identificador == "4" {
			err = arquivo.ItemPedido[indexDetalhamento].ComposeStruct(string(runes))
			indexDetalhamento++
		} else if identificador == "5" {
			err = arquivo.FooterArquivo.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}

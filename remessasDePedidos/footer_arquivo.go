package remessasDePedidos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type FooterArquivo struct {
	TipoRegistro                int32 `json:"TipoRegistro"`
	TotalRegistrosArquivo       int32 `json:"TotalRegistrosArquivo"`
	QuantidadeLinhasItensPedido int32 `json:"QuantidadeLinhasItensPedido"`
}

func (f *FooterArquivo) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesFooterArquivo

	err = posicaoParaValor.ReturnByType(&f.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&f.TotalRegistrosArquivo, "TotalRegistrosArquivo")
	err = posicaoParaValor.ReturnByType(&f.QuantidadeLinhasItensPedido, "QuantidadeLinhasItensPedido")

	return err
}

var PosicoesFooterArquivo = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                {0, 1, 0},
	"TotalRegistrosArquivo":       {1, 7, 0},
	"QuantidadeLinhasItensPedido": {7, 17, 0},
}

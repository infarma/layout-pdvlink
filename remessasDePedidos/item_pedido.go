package remessasDePedidos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type ItemPedido struct {
	TipoRegistro       int32   `json:"TipoRegistro"`
	CodigoProduto      int32   `json:"CodigoProduto"`
	CodigoItemPedido   string  `json:"CodigoItemPedido"`
	QuantidadePedida   int32   `json:"QuantidadePedida"`
	PercentualDesconto float32 `json:"PercentualDesconto"`
	OperadorLogistico  string  `json:"OperadorLogistico"`
	TipoCodigoProduto  int32   `json:"TipoCodigoProduto"`
	TipoDeOperacao     int32   `json:"TipoDeOperacao"`
}

func (i *ItemPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItemPedido

	err = posicaoParaValor.ReturnByType(&i.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&i.CodigoProduto, "CodigoProduto")
	err = posicaoParaValor.ReturnByType(&i.CodigoItemPedido, "CodigoItemPedido")
	err = posicaoParaValor.ReturnByType(&i.QuantidadePedida, "QuantidadePedida")
	err = posicaoParaValor.ReturnByType(&i.PercentualDesconto, "PercentualDesconto")
	err = posicaoParaValor.ReturnByType(&i.OperadorLogistico, "OperadorLogistico")
	err = posicaoParaValor.ReturnByType(&i.TipoCodigoProduto, "TipoCodigoProduto")
	err = posicaoParaValor.ReturnByType(&i.TipoDeOperacao, "TipoDeOperacao")

	return err
}

var PosicoesItemPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":       {0, 1, 0},
	"CodigoProduto":      {1, 14, 0},
	"CodigoItemPedido":   {14, 34, 0},
	"QuantidadePedida":   {34, 39, 0},
	"PercentualDesconto": {39, 43, 2},
	"OperadorLogistico":  {43, 48, 0},
	"TipoCodigoProduto":  {48, 49, 0},
	"TipoDeOperacao":     {49, 50, 0},
}

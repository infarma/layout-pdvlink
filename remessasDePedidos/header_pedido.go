package remessasDePedidos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type HeaderPedido struct {
	TipoRegistro        int32  `json:"TipoRegistro"`
	CnpjCliente         int32  `json:"CnpjCliente"`
	CodigoCliente       string `json:"CodigoCliente"`
	TipoPagamento       int32  `json:"TipoPagamento"`
	PrazoDeterminado    string `json:"PrazoDeterminado"`
	NumeroPedidoCliente string `json:"NumeroPedidoCliente"`
	OrigemDoPedido      string `json:"OrigemDoPedido"`
	ObservacaoPedido    string `json:"ObservacaoPedido"`
	CodigoVendedor      int32  `json:"CodigoVendedor"`
	OperadorLogistico   string `json:"OperadorLogistico"`
	CodigoDoOperador    int32  `json:"CodigoDoOperador"`
}

func (h *HeaderPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesHeaderPedido

	err = posicaoParaValor.ReturnByType(&h.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&h.CnpjCliente, "CnpjCliente")
	err = posicaoParaValor.ReturnByType(&h.CodigoCliente, "CodigoCliente")
	err = posicaoParaValor.ReturnByType(&h.TipoPagamento, "TipoPagamento")
	err = posicaoParaValor.ReturnByType(&h.PrazoDeterminado, "PrazoDeterminado")
	err = posicaoParaValor.ReturnByType(&h.NumeroPedidoCliente, "NumeroPedidoCliente")
	err = posicaoParaValor.ReturnByType(&h.OrigemDoPedido, "OrigemDoPedido")
	err = posicaoParaValor.ReturnByType(&h.ObservacaoPedido, "ObservacaoPedido")
	err = posicaoParaValor.ReturnByType(&h.CodigoVendedor, "CodigoVendedor")
	err = posicaoParaValor.ReturnByType(&h.OperadorLogistico, "OperadorLogistico")
	err = posicaoParaValor.ReturnByType(&h.CodigoDoOperador, "CodigoDoOperador")

	return err
}

var PosicoesHeaderPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":        {0, 1, 0},
	"CnpjCliente":         {1, 15, 0},
	"CodigoCliente":       {15, 35, 0},
	"TipoPagamento":       {35, 36, 0},
	"PrazoDeterminado":    {36, 60, 0},
	"NumeroPedidoCliente": {60, 80, 0},
	"OrigemDoPedido":      {80, 81, 0},
	"ObservacaoPedido":    {81, 281, 0},
	"CodigoVendedor":      {281, 287, 0},
	"OperadorLogistico":   {292, 298, 0},
	"CodigoDoOperador":    {298, 314, 0},
}

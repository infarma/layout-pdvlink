package remessasDePedidos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type DetalhamentoEanSubstituidoItem struct {
	TipoRegistro     int32  `json:"TipoRegistro"`
	CodigoItemPedido string `json:"CodigoItemPedido"`
	ean              int32  `json:"ean"`
}

func (d *DetalhamentoEanSubstituidoItem) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDetalhamentoEanSubstituidoItem

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&d.CodigoItemPedido, "CodigoItemPedido")
	err = posicaoParaValor.ReturnByType(&d.ean, "ean")

	return err
}

var PosicoesDetalhamentoEanSubstituidoItem = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":     {0, 1, 0},
	"CodigoItemPedido": {1, 21, 0},
	"ean":              {21, 34, 0},
}

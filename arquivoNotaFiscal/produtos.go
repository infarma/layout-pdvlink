package arquivoNotaFiscal

type Produtos struct {
	TipoRegistro                          int32   `json:"TipoRegistro"`
	CodigoEan                             int32   `json:"CodigoEan"`
	QuantidadeAtendida                    int32   `json:"QuantidadeAtendida"`
	PercentualDescontoProduto             float32 `json:"PercentualDescontoProduto"`
	ValorUnitarioDesconto                 float32 `json:"ValorUnitarioDesconto"`
	ValorUnitarioLiquidoProduto           float32 `json:"ValorUnitarioLiquidoProduto"`
	BaseCalculoICMS                       float32 `json:"BaseCalculoICMS"`
	BaseCalculoICMSSubstituicaoTributaria float32 `json:"BaseCalculoICMSSubstituicaoTributaria"`
	PercentualICMS                        float32 `json:"PercentualICMS"`
	PercentualIPI                         float32 `json:"PercentualIPI"`
	ValorICMS                             float32 `json:"ValorICMS"`
	ValorST                               float32 `json:"ValorST"`
	ValorICMSRepassado                    float32 `json:"ValorICMSRepassado"`
	CFOP                                  int32   `json:"CFOP"`
	FlagSubstituicaoTributaria            string  `json:"FlagSubstituicaoTributaria"`
	IdentificadorListaPositiva            string  `json:"IdentificadorListaPositiva"`
	PrazoProduto                          int32   `json:"PrazoProduto"`
	ClassificacaoProduto                  int32   `json:"ClassificacaoProduto"`
	DCB                                   string  `json:"DCB"`
	ValorRepasse                          float32 `json:"ValorRepasse"`
	ValorTotalProduto                     float32 `json:"ValorTotalProduto"`
	ValorTotalProdutoMaisEncargos         float32 `json:"ValorTotalProdutoMaisEncargos"`
	CodigoSituacaoTributaria              string  `json:"CodigoSituacaoTributaria"`
	PercentualDescontoFinanceiro          float32 `json:"PercentualDescontoFinanceiro"`
	ValorDescontoFinanceiro               float32 `json:"ValorDescontoFinanceiro"`
	TipoProduto                           string  `json:"TipoProduto"`
}

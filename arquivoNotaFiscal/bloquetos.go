package arquivoNotaFiscal

type Bloquetos struct {
	TipoRegistro                 int32   `json:"TipoRegistro"`
	NumeroBloqueto               int32   `json:"NumeroBloqueto"`
	DataVencimento               int32   `json:"DataVencimento"`
	ValorBloqueto                int32   `json:"ValorBloqueto"`
	DataEmissao                  int32   `json:"DataEmissao"`
	ValorParcela                 float32 `json:"ValorParcela"`
	DescontoAntecipacao          float32 `json:"DescontoAntecipacao"`
	PercentualJurosDia           float32 `json:"PercentualJurosDia"`
	PercentualDescontoFinanceiro float32 `json:"PercentualDescontoFinanceiro"`
	PercentualMulta              float32 `json:"PercentualMulta"`
	CodigoBanco                  string  `json:"CodigoBanco"`
	CodigoAgencia                string  `json:"CodigoAgencia"`
	NumeroContaCorrente          string  `json:"NumeroContaCorrente"`
	ValorTotalDescontoFinanceiro float32 `json:"ValorTotalDescontoFinanceiro"`
}

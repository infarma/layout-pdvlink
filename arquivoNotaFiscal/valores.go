package arquivoNotaFiscal

type Valores struct {
	TipoRegistro       int32   `json:"TipoRegistro"`
	ValorNota          float32 `json:"ValorNota"`
	ValorDescontoNota  float32 `json:"ValorDescontoNota"`
	ValorRepasseICMS   float32 `json:"ValorRepasseICMS"`
	ValorTotalProdutos float32 `json:"ValorTotalProdutos"`
	ValorIPI           float32 `json:"ValorIPI"`
	ValorFrete         float32 `json:"ValorFrete"`
	ValorSeguro        float32 `json:"ValorSeguro"`
}

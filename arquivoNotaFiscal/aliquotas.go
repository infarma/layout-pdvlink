package arquivoNotaFiscal

type Aliquotas struct {
	TipoRegistro    int32   `json:"TipoRegistro"`
	AliquotaICMS    float32 `json:"AliquotaICMS"`
	BaseCalculoICMS float32 `json:"BaseCalculoICMS"`
	ValorICMS       float32 `json:"ValorICMS"`
}

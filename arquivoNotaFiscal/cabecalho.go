package arquivoNotaFiscal

type Cabecalho struct {
	TipoRegistro      int32  `json:"TipoRegistro"`
	SequencialPedido  int32  `json:"SequencialPedido"`
	DataProcessamento int32  `json:"DataProcessamento"`
	HoraProcessamento int32  `json:"HoraProcessamento"`
	DataEmissaoNota   int32  `json:"DataEmissaoNota"`
	CnpjCliente       string `json:"CnpjCliente"`
	SiglaIndustria    string `json:"SiglaIndustria"`
	CnpjFornecedor    string `json:"CnpjDistribuidor"`
}

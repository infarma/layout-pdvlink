package arquivoNotaFiscal

type Finalizador struct {
	TipoRegistro            int32 `json:"TipoRegistro"`
	QuantidadeLinhasArquivo int32 `json:"QuantidadeLinhasArquivo"`
}

package arquivoNotaFiscal

type CabecalhoContinuacao struct {
	TipoRegistro                          int32   `json:"TipoRegistro"`
	NumeroNota                            int32   `json:"CnpjLaboratorio"`
	BaseCalculoICMSSubstituicaoTributaria float32 `json:"BaseCalculoICMSSubstituicaoTributaria"`
	BaseCalculoICMS                       float32 `json:"BaseCalculoICMS"`
	ModeloNotaFiscal                      string  `json:"ModeloNotaFiscal"`
	SerieNotaFiscal                       string  `json:"SerieNotaFiscal"`
	ChaveDANFe                            int32   `json:"ChaveDANFe"`
}

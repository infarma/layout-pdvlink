package arquivoNotaFiscal

type FinalizadorNota struct {
	TipoRegistro         int32   `json:"TipoRegistro"`
	ValorTotalICMS       float32 `json:"ValorTotalICMS"`
	ValorTotalICMSRetido float32 `json:"ValorTotalICMSRetido"`
	QuantidadeVolume     int32   `json:"QuantidadeVolume"`
}

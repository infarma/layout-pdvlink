package arquivoNotaFiscal

type Lotes struct {
	TipoRegistro            string `json:"TipoRegistro"`
	EanProduto              string `json:"EanProduto"`
	Lote                    string `json:"Lote"`
	QuantidadeReferenteLote int32  `json:"QuantidadeReferenteLote"`
	Vencimento              int32  `json:"Vencimento"`
}

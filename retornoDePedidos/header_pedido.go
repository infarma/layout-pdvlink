package remessasDePedidos

type HeaderPedido struct {
	TipoRegistro               int32  `json:"TipoRegistro"`
	NumeroPedidoDistribuidor   string `json:"NumeroPedidoDistribuidor"`
	StatusPedido               int32  `json:"StatusPedido"`
	MotivoNaoAtendimentoPedido int32  `json:"MotivoNaoAtendimentoPedido"`
	TipoImportacao             int32  `json:"TipoImportacao"`
	TipoMudancaStatus          int32  `json:"TipoMudancaStatus"`
	PrazoDeterminado           string `json:"PrazoDeterminado"`
	FasePedido                 string `json:"FasePedido"`
}

package remessasDePedidos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type FooterArquivo struct {
	TipoRegistro                   int32 `json:"TipoRegistro"`
	TotalRegistrosArquivo          int32 `json:"TotalRegistrosArquivo"`
	QuantidadeUnidadesAtendidas    int32 `json:"QuantidadeUnidadesAtendidas"`
	QuantidadeUnidadesNaoAtendidas int32 `json:"QuantidadeUnidadesNaoAtendidas"`
	QuantidadeItensArquivo         int32 `json:"QuantidadeItensArquivo"`
}

func (f *FooterArquivo) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesFooterArquivo

	err = posicaoParaValor.ReturnByType(&f.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&f.TotalRegistrosArquivo, "TotalRegistrosArquivo")
	err = posicaoParaValor.ReturnByType(&f.QuantidadeUnidadesAtendidas, "QuantidadeLinhasItensPedido")

	return err
}

var PosicoesFooterArquivo = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                {0, 1, 0},
	"TotalRegistrosArquivo":       {1, 7, 0},
	"QuantidadeLinhasItensPedido": {7, 17, 0},
}

package remessasDePedidos

type HeaderArquivo struct {
	TipoRegistro     int32 `json:"TipoRegistro"`
	DataHoraArquivo  int32 `json:"DataHoraArquivo"`
	SequencialPedido int32 `json:"SequencialPedido"`
}

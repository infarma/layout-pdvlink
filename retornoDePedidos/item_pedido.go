package remessasDePedidos

type ItemPedido struct {
	TipoRegistro                   int32  `json:"TipoRegistro"`
	CodigoProduto                  int32  `json:"CodigoProduto"`
	CodigoItemPedido               string `json:"CodigoItemPedido"`
	QuantidadeAtendida             int32  `json:"QuantidadeAtendida"`
	QuantidadeNaoAtendida          int32  `json:"QuantidadeNaoAtendida"`
	MotivoNaoAtendimentoItemPedido int32  `json:"MotivoNaoAtendimentoItemPedido"`
	Faturamento                    int32  `json:"Faturamento"`
}
